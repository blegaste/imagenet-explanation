# ImageNet Explanation

SHAP and LIME implementations for explaining models trained on ImageNet.


# Examples

PDF File contains results with two models : ResNet50 and Inception V3. It also contains results for a causal theory based explanation.
